# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Marek Laane <bald@smail.ee>, 2011, 2012, 2013, 2014, 2016.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2019-05-20 03:11+0200\n"
"PO-Revision-Date: 2016-01-13 16:53+0200\n"
"Last-Translator: Marek Laane <qiilaq69@gmail.com>\n"
"Language-Team: Estonian <kde-et@linux.ee>\n"
"Language: et\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.5\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: behavior-config.cpp:50
#, kde-format
msgctxt ""
"Part of config 'show last [spin box] messages' This is the suffix to the "
"spin box. Be sure to include leading space"
msgid " message"
msgid_plural " messages"
msgstr[0] " sõnum"
msgstr[1] " sõnumit"

#: behavior-config.cpp:67
#, kde-format
msgctxt "Placeholder for contact name in completion suffix selector"
msgid "Nickname"
msgstr "Hüüdnimi"

#. i18n: ectx: property (title), widget (QGroupBox, newTabGroupBox)
#: behavior-config.ui:17
#, kde-format
msgid "Tabs"
msgstr "Kaardid"

#. i18n: ectx: property (text), widget (QLabel, label)
#: behavior-config.ui:23
#, kde-format
msgid "Open new conversations:"
msgstr "Uued vestlused avatakse:"

#. i18n: ectx: property (text), widget (QRadioButton, radioNew)
#: behavior-config.ui:30
#, kde-format
msgid "As new windows"
msgstr "Uues aknas"

#. i18n: ectx: property (text), widget (QRadioButton, radioZero)
#: behavior-config.ui:40
#, kde-format
msgid "As tabs in the same window"
msgstr "Kaartidena samas aknas"

#. i18n: ectx: property (title), widget (QGroupBox, scrollbackGroupBox)
#: behavior-config.ui:53
#, kde-format
msgid "Last Conversation Scrollback"
msgstr "Viimase vestluse ajalugu"

#. i18n: First part of &quot;Show last %1 messages&quot; string
#. i18n: ectx: property (text), widget (QLabel, label_p1)
#: behavior-config.ui:59
#, kde-format
msgid "Show last"
msgstr "Näidatakse viimast"

#. i18n: ectx: property (title), widget (QGroupBox, typingGroupBox)
#: behavior-config.ui:85
#, kde-format
msgid "User Is Typing"
msgstr "Kasutaja kirjutab"

#. i18n: ectx: property (text), widget (QCheckBox, checkBoxShowOthersTyping)
#: behavior-config.ui:91
#, kde-format
msgid "Show me when others are typing"
msgstr "Teadaandmine teiste kirjutamisest"

#. i18n: ectx: property (text), widget (QCheckBox, checkBoxShowMeTyping)
#: behavior-config.ui:98
#, kde-format
msgid "Show others when I am typing"
msgstr "Teadaandmine teistele minu kirjutamisest"

#. i18n: ectx: property (title), widget (QGroupBox, nicknameCompletionGroup)
#: behavior-config.ui:108
#, kde-format
msgid "Nickname Completion"
msgstr "Hüüdnime lõpetamine"

#. i18n: ectx: property (text), widget (QLabel, label_2)
#: behavior-config.ui:117
#, kde-format
msgid "Complete nicknames to"
msgstr "Hüüdnimed lõpetatakse"

#. i18n: ectx: property (text), widget (QLabel, label_3)
#: behavior-config.ui:137
#, kde-format
msgid "when I press Tab"
msgstr "tabeldusklahvile vajutamisel"

#. i18n: ectx: property (title), widget (QGroupBox, kbuttongroup)
#: behavior-config.ui:147
#, kde-format
msgid "Image Sharing"
msgstr "Pildijagamine"

#. i18n: ectx: property (text), widget (QLabel, label_4)
#: behavior-config.ui:156
#, kde-format
msgid "Image Sharing Service"
msgstr "Pildijagamisteenus"

#. i18n: ectx: property (text), widget (QLabel, label_5)
#: behavior-config.ui:179
#, kde-format
msgid "Works when dragging an image to the Chat Window"
msgstr "Toimib pildi lohistamisel vestlusaknasse"

#. i18n: ectx: property (title), widget (QGroupBox, groupChatsGroup)
#: behavior-config.ui:195
#, kde-format
msgid "Group Chats"
msgstr "Grupivestlused"

#. i18n: ectx: property (text), widget (QCheckBox, dontLeaveGroupChats)
#: behavior-config.ui:207
#, kde-format
msgid "Stay connected to group chat even when tab is closed"
msgstr "Ühendus grupivestlusega säilitatakse isegi kaardi sulgemisel"

#. i18n: ectx: property (title), widget (QGroupBox, groupBox)
#: behavior-config.ui:217
#, kde-format
msgid "Keyboard Layouts"
msgstr "Klaviatuuripaigutused"

#. i18n: ectx: property (text), widget (QCheckBox, rememberTabKeyboardLayout)
#: behavior-config.ui:223
#, kde-format
msgid "Remember keyboard layout for each tab"
msgstr "Klaviatuuripaigutus jäetakse igal kaardil eraldi meelde"

#~ msgid "A new tab in the last focused window"
#~ msgstr "Uuel kaardil viimati fookuses olnud aknas"

#~ msgid "A demo chat"
#~ msgstr "Näidisvestlus"

#~ msgid "Jabber"
#~ msgstr "Jabber"

#~ msgid "BobMarley@yahoo.com"
#~ msgstr "BobMarley@yahoo.com"

#~ msgid "Bob Marley"
#~ msgstr "Bob Marley"

#~ msgid "larry@example.com"
#~ msgstr "larry@example.com"

#~ msgid "Larry Demo"
#~ msgstr "Larry Demo"

#~ msgid "ted@example.com"
#~ msgstr "ted@example.com"

#~ msgid "Ted Example"
#~ msgstr "Ted Example"

#~ msgid "Hello"
#~ msgstr "Tere"

#~ msgid "A different example message"
#~ msgstr "Teistsugune näidissõnum"

#~ msgid "Ted Example has left the chat."
#~ msgstr "Ted Example lahkus vestlusest."

#~ msgid "Use Custom Font Settings"
#~ msgstr "Kohandatud fondiseadistused"

#~ msgid "Select Font: "
#~ msgstr "Font: "

#~ msgid "Select Size: "
#~ msgstr "Suurus: "

#~ msgid "Style"
#~ msgstr "Stiil"

#~ msgid "Message Style:"
#~ msgstr "Sõnumi stiil:"

#~ msgid "Variant:"
#~ msgstr "Variant:"

#~ msgid "Show User Icons"
#~ msgstr "Kasutaja ikoonide näitamine"

#~ msgid "Telepathy Chat Window Config"
#~ msgstr "Telepathy vestlusakna seadistused"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Marek Laane"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "bald@smail.ee"
